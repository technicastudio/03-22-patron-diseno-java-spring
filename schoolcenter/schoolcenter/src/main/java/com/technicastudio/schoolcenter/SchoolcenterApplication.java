package com.technicastudio.schoolcenter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * The Class SchoolcenterApplication.
 */
@SpringBootApplication
public class SchoolcenterApplication {

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		SpringApplication.run(SchoolcenterApplication.class, args);
	}

}
