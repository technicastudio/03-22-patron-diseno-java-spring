package com.technicastudio.schoolcenter.service;

import com.technicastudio.schoolcenter.model.SchoolListModelResponse;

/**
 * The Interface SchoolService.
 */
public interface SchoolService {

	/**
	 * Filter center.
	 *
	 * @param name the name
	 * @param type the type
	 * @param adress the adress
	 * @return the school list model response
	 */
	public SchoolListModelResponse filterCenter(String name, String type, String adress);

}
