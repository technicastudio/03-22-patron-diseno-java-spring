package com.technicastudio.schoolcenter.mapper;

import java.util.ArrayList;

import org.springframework.stereotype.Component;

import com.technicastudio.schoolcenter.entity.SchoolEntityResponse;
import com.technicastudio.schoolcenter.entity.SchoolListEntityResponse;
import com.technicastudio.schoolcenter.model.SchoolListModelResponse;
import com.technicastudio.schoolcenter.model.SchoolModelResponse;

/**
 * The Class SchoolMapper.
 */
@Component
public class SchoolMapper {

	/**
	 * Creates the school list model response.
	 *
	 * @param filteredEntityResponse the filtered entity response
	 * @return the school list model response
	 */
	public SchoolListModelResponse createSchoolListModelResponse(SchoolListEntityResponse filteredEntityResponse) {
		SchoolListModelResponse response = new SchoolListModelResponse();
		response.setList(new ArrayList<>());
		for(SchoolEntityResponse schoolEntity: filteredEntityResponse.getList()) {
			SchoolModelResponse modelResponse = new SchoolModelResponse();
			modelResponse.setName(schoolEntity.getName());
			modelResponse.setType(schoolEntity.getType());
			modelResponse.setAddress(schoolEntity.getAddress());
			
			response.getList().add(modelResponse);
		}
		return response;
	}

}
