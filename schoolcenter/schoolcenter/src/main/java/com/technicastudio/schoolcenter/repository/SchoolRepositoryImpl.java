package com.technicastudio.schoolcenter.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import com.technicastudio.schoolcenter.entity.SchoolListEntityResponse;

/**
 * The Class SchoolRepositoryImpl.
 */
@Repository
public class SchoolRepositoryImpl implements SchoolRepository {

	/** The school center url. */
	@Value("${school-repository.center-url}")
	private String schoolCenterUrl;

	/** The rest template. */
	@Autowired
	private RestTemplate restTemplate;

	

	/**
	 * Find all.
	 *
	 * @return the school list entity response
	 */
	@Override
	public SchoolListEntityResponse findAll() {

		SchoolListEntityResponse response = restTemplate.getForObject(schoolCenterUrl, SchoolListEntityResponse.class);

		return response;
	}

}
