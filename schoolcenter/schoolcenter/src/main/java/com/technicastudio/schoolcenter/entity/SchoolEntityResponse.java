package com.technicastudio.schoolcenter.entity;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * The Class SchoolEntityResponse.
 */
public class SchoolEntityResponse {

	/** The name. */
	@JsonProperty("centro_nombre")
	private String name;
	
	/** The type. */
	@JsonProperty("centro_titularidad")
	private String type;
	
	/** The address. */
	@JsonProperty("direccion_via_nombre")
	private String address;
	
	/** The codigo municipio. */
	@JsonProperty("municipio_codigo")
	private String codigoMunicipio;
	
	/** The direccion via tipo. */
	@JsonProperty("direccion_via_tipo")
	private String direccionViaTipo;

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the type.
	 *
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * Sets the type.
	 *
	 * @param type the new type
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * Gets the address.
	 *
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * Sets the address.
	 *
	 * @param address the new address
	 */
	public void setAddress(String address) {
		this.address = address;
	}



}
